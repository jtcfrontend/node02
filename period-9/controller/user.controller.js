var fs = require('fs');
var path = require('path');
var User = require('./../models/user.model');

module.exports = {
    getUsers: getUsers,
    createUser: createUser
}

function getUsers() {
    return User.find()
        .then(function (users) {
            return Promise.resolve(users);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function createUser(newUser) {
    var user = new User(newUser);

    return user.save()
        .then(function (user) {
            return Promise.resolve(user);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}