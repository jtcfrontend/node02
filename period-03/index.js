var fs = require("fs");
console.log("Chuan bi ghi du lieu vao file hien tai");
fs.writeFile('input.txt', 'Hoc Node.js co ban tai JTC!', function (err) {
    if (err) {
        return console.error(err);
    }
    console.log("Ghi du lieu vao file thanh cong!");
    console.log("Doc du lieu vua duoc ghi");
    fs.readFile('input.txt', function (err, data) {
        if (err) {
            return console.error(err);
        }
        console.log("Phuong thuc doc file khong dong bo: " + data.toString());
    });
});