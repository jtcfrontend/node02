var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');
var path = require('path');

router.get('', getUser);

module.exports = router;


function getUser(req, res) {
    fs.readFile(path.join(__dirname, "../user.json"), "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            res.send(formattedData);
        }
    })
}