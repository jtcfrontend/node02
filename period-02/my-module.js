module.exports = {
    tinhChuVi: chuvi,
    inKetQua: InKq,
    tinhDienTich: dientich
}

function chuvi(a, b, c, callback) {
    var s = a + b + c;
    callback(s);
}

function InKq(data) {
    console.log(data);
}

function dientich(a, b, c, callback) {
    var s = a * b * c;
    callback(s);
}