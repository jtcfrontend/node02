var router = require('express').Router();
var fs = require('fs');
var path = require('path');
var jwt = require('../utils/jwt');

router.post('/login', login);

module.exports = router;

function login(req, res) {
    var name = req.body.name;
    var password = req.body.password;

    fs.readFile(path.join(__dirname, "../user.json"), 'utf8', function (err, data) {
        var list = JSON.parse(data);
        var user;
        for (var i = 0; i < list.length; i++) {
            if (list[i].name == name && list[i].password == password) {
                user = list[i];
            }
        }

        if (user) {
            jwt.sign({
                name: user.name
            }, function (err, token) {
                if (err) {
                    res.status(400);
                    res.json({
                        message: 'Name or password is incorrect'
                    })
                } else {
                    res.json({ access_token: token});
                }
            })
        } else {
            res.status(400);
            res.json({
                message: 'Name or password is incorrect'
            })
        }

    });
}