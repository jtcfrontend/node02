var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');
var path = require('path');
var auth = require('./../middle-ware/auth');

router.get('', getUser);
router.put('/:id',auth.auth(), updateUser);
router.get('/:id', getUserById);
router.post('', createUser);
router.delete('/:id', deleteUser);

module.exports = router;


function deleteUser(req, res) {
    var id = req.params.id;
    var exist = false;

    fs.readFile(path.join(__dirname, "../user.json"), "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            for (var i = 0; i < formattedData.length; i++) {
                if (formattedData[i].id == id) {
                    formattedData.splice(i, 1);
                    exist = true;
                    fs.writeFile(path.join(__dirname, "../user.json"), JSON.stringify(formattedData), function (err, data) {
                        if (err) {
                            res.status(500);
                            res.send(err.message);
                        } else {
                            res.send("OK");
                        }
                    })

                    break;
                }
            }
            if (!exist) {
                res.status(404);
                res.send("Not Found");
            }
        }
    })
}

function getUserById(req, res, next) {
    var id = req.params.id;
    console.log('here');
    fs.readFile(path.join(__dirname, "../user1.json"), "utf8", function (err, data) {
        if (err) {
            // res.status(404);
            // res.send(err.message);
            next(err);
        } else {
            var formattedData = JSON.parse(data);
            for (var i = 0; i < formattedData.length; i++) {
                if (formattedData[i].id == id) {
                    res.send(formattedData[i]);
                    return;
                }
            }

            res.status(404);
            res.send("Not Found");
        }
    })
}

function createUser(req, res) {
    var user = req.body;
    console.log(user);

    if (!user.username) {
        res.status(400);
        res.end("Username is required");
    }

    fs.readFile(path.join(__dirname, "../user.json"), "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            formattedData.push(user);

            fs.writeFile(path.join(__dirname, "../user.json"), JSON.stringify(formattedData), function (err) {
                if (err) {
                    res.status(500);
                    res.send(err.message);
                } else {
                    res.send(user);
                }
            })
        }
    })
}

function getUser(req, res, next) {
    fs.readFile(path.join(__dirname, "../user.json"), "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            res.send(formattedData);
        }
    })
}

function updateUser(req, res , next) {
    var user = req.body;
    var id = req.params.id;
    console.log(user);

    if (!user.username) {
     return next({statusCode: 400, message: "User id required"});
    }

    fs.readFile(path.join(__dirname, "../user.json"), "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            var foundUserIndex;

            for (var i = 0; i < formattedData.length; i++) {
                if (formattedData[i].id == id) {
                    foundUserIndex = i;
                    break;
                }
            }

            if (foundUserIndex == undefined) {
                res.status(404);
                res.send("Not Found");
            } else {
                for (var p in user) {
                    formattedData[foundUserIndex][p] = user[p];
                }

                fs.writeFile(path.join(__dirname, "../user.json"), JSON.stringify(formattedData), function (err) {
                    if (err) {
                        res.status(500);
                        res.send(err.message);
                    } else {
                        res.send(formattedData[foundUserIndex]);
                    }
                })
            }
        }
    })
}